# PixActivities


ExcelDataReader: https://github.com/ExcelDataReader/ExcelDataReader The MIT License (MIT) Copyright (c) 2014 ExcelDataReader

EPPlus Version 4.5.3.3: https://github.com/JanKallman/EPPlus (LGPL)

Jint: https://github.com/sebastienros/jint  Copyright (c) 2013, Sebastien Ros