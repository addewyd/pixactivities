﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BR.Core;
using BR.Core.Attributes;
using ExcelDataReader;
using System.Data;

namespace Activities.TL
{
    [ScreenName("Excel Reader")]
    [Representation("Excel Reader")]
    [Path("TL.Excel.Reader")]
    [Description("Excel Reader")]
    [OptionList("Open", "SetList", "ReadNext", "Close", "Tables")]
    class ExcelReader : Activity
    {
        static IExcelDataReader reader = null;
        static System.IO.FileStream stream = null;
        static int fieldCount = 0;

        [Description("FileName")]
        [IsRequired]
        [Options(0)]
        public string FileName { get; set; }

        [Description("ListName")]
        [IsRequired]
        [Options(1)]
        public string ListName { get; set; }

        [Description("ListFound")]
        [IsRequired]
        [IsOut]
        [Options(1)]
        public string ListFound { get; set; }

        [Description("FieldCount")]
        [IsRequired]
        [IsOut]
        [Options(1)]
        public int FieldCount { get; set; }

        [Description("RowCount")]
        [IsRequired]
        [IsOut]
        [Options(1)]
        public int RowCount { get; set; }

        [Description("Row")]
        [IsRequired]
        [IsOut]
        [Options(2)]
        public List<object> Row { get; set; }

        [Description("Table")]
        [IsRequired]
        [IsOut]
        [Options(4)]
        public List<DataTable> Tables { get; set; }


        public override void Execute(int? optionID)
        {
            // mode Open
            if(optionID == 0)
            {
                if(stream != null)
                {
                    stream.Close();
                    stream = null;
                }
                stream = System.IO.File.Open(FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                reader = ExcelReaderFactory.CreateReader(stream);

            }

            // .............................................................................

            if(optionID == 1)
            {
                FieldCount = 0;
                RowCount = 0;
                int rc = reader.ResultsCount;
                for(int i = 0; i < rc; i ++)
                {

                    if(reader.Name == ListName)
                    {
                        ListFound = ListName;
                        fieldCount = reader.FieldCount;
                        FieldCount = fieldCount;
                        RowCount = reader.RowCount;
                        break;
                    }
                    var r = reader.NextResult();
                    if(!r)
                    {
                        // list not found
                        ListFound = null;
                    }
                }
            }

            // .............................................................................

            if (optionID == 2)
            {
                var r = reader.Read();
                if (!r)
                {
                    Row = null;
                    stream.Close();
                } else
                {
                    List<object> row = new List<object>();

                    for(int i = 0; i < fieldCount; i ++)
                    {
                        var x = reader.IsDBNull(i) ? "" : reader.GetValue(i);
                        
                        row.Add(x);
                    }
                    Row = row;
                }
            }

            if(optionID == 3)
            {
                stream.Close();
                stream = null;
            }

            // table
            if(optionID == 4)
            {
                Tables = new List<DataTable>();
                var ds = reader.AsDataSet();
                var dt = ds.Tables;
                foreach(DataTable t in dt)
                {
                    Tables.Add(t);
                }
            }
        }
    }
}
