﻿using System;
using System.Runtime;
using BR.Core;
using BR.Core.Attributes;
using System.Collections.Generic;

namespace Activities.TL
{
    static class Storage
    {
        public static Dictionary<string, object> t = new Dictionary<string, object>();
    }

    [ScreenName("Dict Storage")]
    [Representation("Save/Restore [ValName]")]
    [Path("TL.Storages")]
    [Description("static dict storage")]

    class Static01 : Activity
    {
        [ScreenName("Name")]
        [Description("Name")]
        [IsRequired]
        public string ValName { get; set; }

        [ScreenName("SetVal")]
        [Description("SetVal")]
        public object SetVal { get; set; }

        [ScreenName("GetVal")]
        [Description("GetVal")]
        [IsOut]
        public object GetVal { get; set; }

        [ScreenName("Storage")]
        [Description("Storage")]
        [IsOut]
        public Dictionary<string, object> Debug { get; set; }


        public override void Execute(int? optionID)
        {
            if(SetVal != null)
            {
                Storage.t[ValName] = SetVal;
            }
            try
            {
                GetVal = Storage.t[ValName];
            } catch(Exception)            
            {
                GetVal = null;
            }
            Debug = Storage.t;
        }
    }
}
