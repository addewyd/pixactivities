﻿using System;
using System.Runtime;
using BR.Core;
using BR.Core.Attributes;
using System.Collections.Generic;

namespace Activities.TL
{
    static class StackStorage
    {
        public static Stack<object> t = new Stack<object>();
    }

    [ScreenName("Stack Storage")]
    [Representation("Push/Pop")]
    [Path("TL.Storages")]
    [Description("static stack storage")]

    class Static02 : Activity
    {

        [ScreenName("SetVal")]
        [Description("SetVal")]
        public object SetVal { get; set; }

        [ScreenName("GetVal")]
        [Description("GetVal")]
        [IsOut]
        public object GetVal { get; set; }

        [ScreenName("Storage")]
        [Description("Storage")]
        [IsOut]
        public Stack<object> Debug { get; set; }

 
        public override void Execute(int? optionID)
        {
            if (SetVal != null)
            {
                StackStorage.t.Push(SetVal);
                GetVal = StackStorage.t.Peek();
            } else {
                GetVal = StackStorage.t.Pop();
            }

            Debug = StackStorage.t;
        }
    }
}
