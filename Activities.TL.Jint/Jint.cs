﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BR.Core;
using BR.Core.Attributes;
using Jint;

namespace Activities.TL.Jint
{
    [ScreenName("Execute")]
    [Representation("Execute [Script] Result [Result]")]
    [BR.Core.Attributes.Path("TL.Jint")]
    [Description("Execute")]

    public class JSExecute : Activity
    {
        [ScreenName("Script")]
        [Description("Script")]
        [IsRequired]
        [IsCodeEditor]
        public string Script { get; set; }

        [ScreenName("Parameters")]
        [Description("Parameters")]
        [IsRequired]
        public Dictionary<string, object> Parameters { get; set; } = null;

        [ScreenName("Result")]
        [Description("Result")]
        [IsOut]
        public object Result { get; set; }

        [ScreenName("TypeName")]
        [Description("TypeName")]
        [IsOut]
        public string TypeName { get; set; }

        override public void Execute(int? opt)
        {
            var engine = new Engine(cfg => cfg.AllowClr());
            
            if (Parameters != null)
            {
                foreach(var Kv in Parameters)
                {
                    engine.SetValue(Kv.Key, Kv.Value);
                }
            }
            engine.Execute(Script);
            Result = engine.GetCompletionValue().ToObject();
            TypeName = Result?.GetType()?.Name;
        }
    }
}
