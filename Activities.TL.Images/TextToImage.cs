﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BR.Core;
using BR.Core.Attributes;
using Images.Core;

namespace Activities.TL
{
    [ScreenName("Text to Image")]
    [Representation("Text [TextString] to Image [Image]")]
    [BR.Core.Attributes.Path("TL.Images")]
    [Description("Text to Image")]

    class TextToImage : Activity
    {
        [ScreenName("Text")]
        [Description("Text")]
        [IsRequired]

        public string TextString { get; set; }

        [ScreenName("Font Size")]
        [Description("Font Size")]
        [IsRequired]

        public int FontSize { get; set; } = 12;

        [ScreenName("Font Name")]
        [Description("Font Name")]
        [IsRequired]

        public string FontName { get; set; }

        [ScreenName("Text Color")]
        [Description("Text Color")]
        public object TextColor { get; set; } = null;

        [ScreenName("Background Color")]
        [Description("Background Color")]
        public object BackgroundColor { get; set; } = null;

        [ScreenName("Bold")]
        [Description("Bold")]
        [IsCheckBox]
        [IsRequired]
        public bool Bold { get; set; } = false;

        [ScreenName("Italic")]
        [Description("Italic")]
        [IsRequired]
        [IsCheckBox]
        public bool Italic { get; set; } = false;

        [ScreenName("Underline")]
        [Description("Underline")]
        [IsRequired]
        [IsCheckBox]
        public bool Underline { get; set; } = false;

        [ScreenName("Strikeout")]
        [Description("Strikeout")]
        [IsRequired]
        [IsCheckBox]
        public bool Strikeout { get; set; } = false;

        [ScreenName("Image")]
        [Description("Image")]
        [IsRequired]
        [IsOut]
        public Picture Image {get; set;}

        override public void Execute(int? opt)
        {
            Color bc = Color.White;
            Color tc = Color.Black;

            if(TextColor != null)
            {
                var tfn = TextColor.GetType().FullName;
                if (tfn == "System.Drawing.Color")
                {
                    tc = (Color)TextColor;
                }
                else if (tfn == "System.String")
                {
                    if ((TextColor as string).Substring(0, 1) == "#")
                    {
                        tc = ColorTranslator.FromHtml(TextColor as string);
                    } else
                    {
                        tc = Color.FromName(TextColor as string);
                    }
                }
                else if (tfn == "System.Int64" || tfn == "System.Int32")
                {
                    tc = ColorTranslator.FromHtml("#" + ((int)TextColor).ToString("X"));
                }
                else { }

            }
            if (BackgroundColor != null)
            {
                var tfn = BackgroundColor.GetType().FullName;
                if (tfn == "System.Drawing.Color")
                {
                    bc = (Color)BackgroundColor;
                }
                else if (tfn == "System.String")
                {
                    if ((BackgroundColor as string).Substring(0, 1) == "#")
                    {
                        bc = ColorTranslator.FromHtml(BackgroundColor as string);
                    }
                    else
                    {
                        bc = Color.FromName(BackgroundColor as string);
                    }
                }
                else if (tfn == "System.Int64" || tfn == "System.Int32")
                {
                    bc = ColorTranslator.FromHtml("#" + ((int)BackgroundColor).ToString("X"));
                }
                else { }
            }

            Bitmap bitmap = new Bitmap(1, 1);

            var fs = FontStyle.Regular;
            if (Bold)
            {
                fs |= FontStyle.Bold;
            }
            if (Italic)
            {
                fs |= FontStyle.Italic;
            }
            if (Underline)
            {
                fs |= FontStyle.Underline;
            }

            if (Strikeout)
            {
                fs |= FontStyle.Strikeout;
            }
            Font font = new Font(FontName, FontSize, fs, GraphicsUnit.Pixel);
            try
            {
                Graphics graphics = Graphics.FromImage(bitmap);
                int width = (int)graphics.MeasureString(TextString, font).Width;
                int height = (int)graphics.MeasureString(TextString, font).Height;
                bitmap = new Bitmap(bitmap, new Size(width, height));
                graphics = Graphics.FromImage(bitmap);
                graphics.Clear(bc);
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
                graphics.DrawString(TextString, font, new SolidBrush(tc), 0, 0);
                graphics.Flush();
                graphics.Dispose();
                Image = new Picture(bitmap);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
