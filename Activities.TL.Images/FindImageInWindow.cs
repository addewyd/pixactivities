﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BR.Core;
using BR.Core.Attributes;
using Images.Core;
using System.Runtime.InteropServices;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using Emgu.CV.Util;


namespace Activities.TL
{

    class Locs
    {
        public double v { get; set; }
        public Point p { get; set; }
    }

    [ScreenName("Find Image")]
    [Representation("Find image [What] in image [Where] with treshold [Coeff]")]
    [BR.Core.Attributes.Path("TL.Images")]
    [Description("Find Image.  For SDIFF_NORMED MinValue is better")]
    [OptionList("One", "All")]

    public class FindImageInWindow : Activity
    {
        [ScreenName("What to find")]
        [Description("What to find")]
        [IsRequired]
        public Picture What { get; set; }

        [ScreenName("Where to find")]
        [Description("Where to find")]
        [IsRequired]
        public Picture Where { get; set; }

        [ScreenName("Coeff")]
        [Description("Coeff")]
        [IsRequired]
        public double Coeff { get; set; } = 0.7;

        [ScreenName("Compare method")]
        [Description("Compare method")]
        [IsRequired]
        public TemplateMatchingType MatchingType { get; set; } = TemplateMatchingType.CcoeffNormed;

        [ScreenName("Rectangle")]
        [Description("Coords")]
        [IsRequired]
        [IsOut]
        [Options(0)]
        public Rectangle Coords { get; set; }

        [ScreenName("Convert to B/W")]
        [Description("Convert to B/W")]
        [IsRequired]
        [IsCheckBox]
        public bool Cbw { get; set; } = false;

        [ScreenName("MaxValue or MinValue (for SQDIFF)")]
        [Description("MaxValue or MinValue (for SQDIFF)")]
        //[IsRequired]
        [Options(0)]
        [IsOut]
        public double  MaxValue { get; set; }

        [ScreenName("Rectangles")]
        [Description("Coord List")]
        [IsRequired]
        [IsOut]
        [Options(1)]
        public List<Rectangle>  CoordsList { get; set; }

        [ScreenName("MaxValue or MinValue (for SQDIFF)")]
        [Description("MaxValue or MinValue (for SQDIFF)")]
        [Options(1)]
        [IsOut]
        public List<double> MaxValues { get; set; }

        override public void Execute(int? opt)
        {
            Image<Bgr, byte> what = new Image<Bgr, byte>(What.Bitmap);
            Image<Bgr, byte> where = new Image<Bgr, byte>(Where.Bitmap);
            Image<Gray, Byte> whereg = where.Convert<Gray, Byte>();
            Image<Gray, Byte> whatg = what.Convert<Gray, Byte>();

            using (Image<Gray, float> result = Cbw ? (whereg.MatchTemplate(whatg, MatchingType)) : (where.MatchTemplate(what, MatchingType)))
            {
                result.MinMax(out double[] minValues, out double[] maxValues, out Point[] minLocations, out Point[] maxLocations);
                MaxValue = maxValues[0];
                
                if (opt == 1)
                {
                    CoordsList = new List<Rectangle>();
                    MaxValues = new List<double>();
                }
                if(MatchingType == TemplateMatchingType.Ccoeff || MatchingType == TemplateMatchingType.Sqdiff || MatchingType == TemplateMatchingType.Ccorr)
                {
                    throw new Exception("This matching type is not supported");
                }
                if (MatchingType == TemplateMatchingType.SqdiffNormed)
                {
                    if (opt == 0)
                    {
                        MaxValue = minValues[0];
                        if (minValues[0] < Coeff)
                        {
                            Coords = new Rectangle(minLocations[0], what.Size);
                        }
                    }
                    else if (opt == 1)
                    {
                        for(int r = 0; r < result.Rows; r ++)
                        {
                            for(int c = 0; c < result.Cols; c ++)
                            {
                                if(result[r, c].Intensity < Coeff)
                                {
                                    CoordsList.Add(new Rectangle(new Point(c, r), what.Size));
                                    MaxValues.Add(result[r, c].Intensity);
                                }
                            }
                        }
                    }
                    else { }                    
                }
                else
                {
                    if (opt == 0)
                    {
                        if (maxValues[0] > Coeff)
                        {
                            Coords = new Rectangle(maxLocations[0], what.Size);
                        }
                    }
                    else if (opt == 1)
                    {
                        for (int r = 0; r < result.Rows; r++)
                        {
                            for (int c = 0; c < result.Cols; c++)
                            {
                                if (result[r, c].Intensity > Coeff)
                                {
                                    CoordsList.Add(new Rectangle(new Point(c, r), what.Size));
                                    MaxValues.Add(result[r, c].Intensity);
                                }
                            }
                        }
                    }
                    else { }
                }
            }
        }
    }
}
