﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using BR.Core;
using BR.Core.Attributes;
using System.Drawing;
using Images.Core;

namespace Activities.TL
{
    class ProcData
    {
        public Color c { get; set; }
        public double f { get; set; }
        public Rectangle r { get; set; }
        public double m { get; set; }
    }

    [ScreenName("Draw Rectangle")]
    [Representation("Draw [Coords] with [BorderColor] and wdth [BorderWidth]")]
    [BR.Core.Attributes.Path("TL.Images")]
    [Description("Draw rect")]

    public class SomeDraw : Activity
    {
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr GetDC(IntPtr hWnd);
        [DllImport("user32.dll", SetLastError = true)]
        static extern int ReleaseDC(IntPtr hWnd, IntPtr dc);

        void Proc(object p)
        {
            ProcData pd = p as ProcData;
            var dc = GetDC(IntPtr.Zero);
            if (dc != IntPtr.Zero)
            {
                using (Graphics g = Graphics.FromHdc(dc))
                using (Pen pen = new Pen(pd.c, (float)(pd.f)))
                {
                    for (int i = 0; i <= pd.m / 10; i++)
                    {
                        g.DrawRectangle(pen, pd.r);
                        Thread.Sleep(10);
                    }
                }
                ReleaseDC(IntPtr.Zero, dc);
            }
        }

        [ScreenName("Coords")]
        [Description("Coords")]
        [IsRequired]
        public Rectangle Coords { get; set; }

        [ScreenName("Border color")]
        [Description("Border color")]
        [IsRequired]
        public System.Drawing.Color BorderColor { get; set; } = Color.Red;

        [ScreenName("Border width")]
        [Description("Border width")]
        [IsRequired]
        public double BorderWidth { get; set; } = 1.5;

        [ScreenName("Min Time ms")]
        [Description("Min Time ms")]
        [IsRequired]
        public double BorderTime { get; set; } = 1000;

        // ..............................................................................

        public override void Execute(int? optionID)
        {
            var p = new ProcData()
            {
                c = BorderColor,
                m = BorderTime,
                r = new Rectangle(Coords.Location, Coords.Size),
                f = BorderWidth
            };
            var t = new Thread(Proc);
            t.Start(p);
        }
    }

    // ..............................................................................

    [ScreenName("Show Image")]
    [Representation("Show [Image]")]
    [BR.Core.Attributes.Path("TL.Images")]
    [Description("Show Image")]

    public class ShowImage : Activity
    {
        [ScreenName("Image to Show")]
        [Description("Image to Show")]
        [IsRequired]
        public Picture Image { get; set; }

        [ScreenName("WindowName")]
        [Description("WindowName")]
        [IsRequired]
        public string WindowName { get; set; }

        [ScreenName("Wait ms")]
        [Description("Wait ms")]
        [IsRequired]
        public int Wait { get; set; } = 0;

        // ..............................................................................

        public override void Execute(int? optionID)
        {
            var i = new Emgu.CV.Image<Emgu.CV.Structure.Bgr, byte>(Image.Bitmap);
            Emgu.CV.CvInvoke.NamedWindow(WindowName, Emgu.CV.CvEnum.NamedWindowType.Normal);
            Emgu.CV.CvInvoke.Imshow(WindowName, i);
            Emgu.CV.CvInvoke.WaitKey(Wait);
            Emgu.CV.CvInvoke.DestroyWindow(WindowName);
        }
    }
}
