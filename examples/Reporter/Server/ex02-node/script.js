﻿const ws = new WebSocket("ws://#IP:#PORT");

ws.onopen = function() {
    ws.send("open connect");
};

ws.onmessage = function(event) {  
  showMessage(event.data);
};

ws.onerror = function(event) {  
  showError(event.data);
};

function showMessage(message) {
    console.log(message);
    let j = JSON.parse(message);
    let info = "R1";
    let info2 = "R2";
    let info3 = "R3";
    let show1 = false;
    let show2 = false;
    let show3 = false;
    for(let k of Object.keys(j)) {
        if(k.match(/2$/)) {
            info2 += `
            <div class="b">
                <i><span class="g">${k}</span></i> = 
                <span class="h">${j[k]}</span>
            </div>`;
            show2 = true;
        }
        else if(k.match(/3$/)) {
            info3 += `
            <div class="b">
                <i><span class="g">${k}</span></i> = 
                <span class="h">${j[k]}</span>
            </div>`;
            show3 = true;
        } else {
            info += `
            <div class="b">
                <i><span class="g">${k}</span></i> = 
                <span class="h">${j[k]}</span>
            </div>`;
            show1 = true;
        }
    }

    if(show1) document.getElementById('msg').innerHTML = info;
    if(show2) document.getElementById('msg2').innerHTML = info2;
    if(show3) document.getElementById('msg3').innerHTML = info3;
}

function showError(message) {
    document.getElementById('msg').innerHTML = message;
}

function SendStop() {
    ws.send("Stop");
}
