﻿const express = require('express');
const WS = require('ws');
const bodyParser = require('body-parser')
const fs = require("fs")

let config = JSON.parse(fs.readFileSync('server-config.json'))

let scriptf = config.script
let indexf = config.index
let  cssf = config.css

let pid = process.pid

fs.writeFileSync("_pid", Buffer.from("" + pid));

const app = express();
 
let file = fs.readFileSync(indexf)
let script = fs.readFileSync(scriptf).toString()
//let css = fs.readFileSync(cssf)

let lastmessage = ""

script = script.replace(/#IP/, config.wsaddr).replace(/#PORT/, config.wsport)

app.use(express.static(__dirname + '/'));
app.use(bodyParser.json())

app.get('/', (req, res, next) => {
    console.log('get');
    res.send(file.toString())
    res.end();
});


app.get('/script', (req, res, next) => {
    console.log('get script');
    res.send(script)
    res.end();
});
 
let clients = {}

app.post('/x', (req, res, next) => {
    console.log("got post x");
    let c = req.body
    console.log(c);
    for(let key in clients) {
        let wsg = clients[key]
        if(wsg) 

            // pix script dependent
            // piz (17) finished
            if(c.Status && c.Status == "Finish") {
                // clar last message
                lastmessage = ""
            }

            try {
                // resend data to browser
                wsg.send(JSON.stringify(c))
            } catch(e) {
                console.log(e.message)
            }
        }
    }
    // send response: command to http client
    res.send(lastmessage);
    res.end();
//    lastmessage = ""
});

app.post('/y', (req, res, next) => {
    console.log("got post y");
    let c = req.body
    for(let key in clients) {
        let wsg = clients[key]
        if(wsg) {
            try {
                // resend data from chttp lient
                wsg.send(JSON.stringify(c))
            } catch(e) {
                console.log(e.message)
            }
        }
    }
    res.send("ok");
    res.end();
});

let wsserver = new WS.Server({ port: config.wsport });

wsserver.on('connection', ws => {

    let id = Math.random();
    clients[id] = ws;
    console.log("New conn " + id);

    ws.on('message', message => {
        console.log('Got msg ' + message);

        // some command or "connect established"
        lastmessage = message

        for(let key in clients) {
            let info = `{"Id": ${id}}`;
            console.log(info);

            //send connect OK
            clients[key].send(info);
            // test
            let info2 = `{"Id2": ${id}}`;
            clients[key].send(info2);
        }
    });

    ws.on('close', () => {
        console.log('Conn closed ' + id);
        delete clients[id];
    });

});
 
app.listen(config.httpport);
