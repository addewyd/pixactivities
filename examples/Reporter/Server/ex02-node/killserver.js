﻿const fs = require("fs")
const p = require("ps-list")


let pid
try {
    pid= fs.readFileSync("_pid").toString().replace(/\n/, '');
} catch(e) {
    console.log(e.message)
    process.exit(0)
}
console.log(pid)

p().then(data => {
    let f = data.filter(d => {
        return d.name == 'node.exe'
    })

    console.log(f);
    f.forEach(x => {
        if(x.pid == pid) {
            // kill it
            try {
                process.kill(pid)
                console.log("Killed " + pid)
                fs.unlinkSync("_pid")
            } catch(e) {
                console.log(e.message)
            }
        }
    })

});