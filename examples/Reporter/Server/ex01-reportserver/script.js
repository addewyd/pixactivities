﻿const ws = new WebSocket("ws://localhost:4649/Reporter");

ws.onopen = function() {
    ws.send("open");
};

ws.onmessage = function(event) {  
  showMessage(event.data);
    ws.send("message");
};

ws.onerror = function(event) {  
  showError(event.data);
};

function showMessage(message) {
    console.log(message);
//  {"Cnt": "4", "Str": "xxx40", "DateTime": "20.12.2020 6:12:45", "Inner cycle 2": "Counts: 5 102", "Response 2": ""}
    let msgj = JSON.parse(message)
    if(msgj.ID) {
        document.getElementById('connid').innerHTML = "ID: " + msgj.ID;
    } else {
        document.getElementById('cnt').innerHTML = '<div class="s">Cnt: </div>' + msgj["Cnt"];
        document.getElementById('datetime').innerHTML = '<div class="s">DateTime: </div>' + msgj["DateTime"];
        document.getElementById('innercycle').innerHTML = '<div class="s">Counters: </div>' + msgj["Inner cycle 2"];
        document.getElementById('command').innerHTML = '<div class="s">Command: </div>' + msgj["Response 2"];
    }
}

function showError(message) {
    document.getElementById('connid').innerHTML = message;
}


function sendcommand() {
    let cmd = document.getElementById('cmdinp').value;
    console.log(cmd);
    ws.send(cmd);
}