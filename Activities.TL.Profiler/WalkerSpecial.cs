﻿using System;
using BR.Core;
using BR.Core.Attributes;

namespace Activities.TL
{
    [ScreenName("PIX Script Walker -special edition")]
    [Representation("[ScriptFile] -> Logic File or String")]
    [Path("TL.Utils")]
    [Description("walker converts PIX script to text - PIX Script Walker - special edition) for pjc")]
    [OptionList("To File", "To String")]

    public class WalkerSpecial : Activity
    {
        int stepcounter = 0;

        [ScreenName("ScriptFile")]
        [Description("ScriptFile")]
        [IsRequired]
        [IsFilePathChooser]
        [Options(0, 1)]
        public string ScriptFile { get; set; }

        [ScreenName("Logic")]
        [Description("Logic")]
        [IsFilePathChooser]
        [Options(0)]
        public string ScLogic { get; set; }

        [ScreenName("Logic")]
        [Description("Logic")]
        [Options(1)]
        [IsOut]
        public string ScStrLogic { get; set; }

        [ScreenName("StepCounter")]
        [Description("StepCounter")]
        [Options(0, 1)]
        [IsOut]
        public int StepCounter { get; set; }

        private void writeiline(System.IO.TextWriter sv, int level, String s)
        {
            sv.WriteLine(s.PadLeft(s.Length + level * 4));
        }

        public override void Execute(int? optionID)
        {
            var x = System.IO.File.ReadAllText(ScriptFile);
            var t = BR.Logic.Serializer.GetObjectFromXML<BR.Logic.Script>(x);
            var k = t.Logic.Childs.Count.ToString();

            var w = new BR.Logic.ScriptWalker(t);
            var l = w.AllSteps;
            var s = w.Script;
            int level = 0;
            StepCounter = 0;
            stepcounter = 0;

            System.IO.TextWriter sv;
            if (optionID == 0)
            {
                sv = System.IO.File.CreateText(ScLogic);
            }
            else
            {
                sv = new System.IO.StringWriter();
            }
            try
            {
                sv.WriteLine(ScriptFile + ":\n");
                var sparams = s.Params;
                try
                {
                    foreach (var step in l)
                    {
                        string act = "";
                        try
                        {
                            level = step.StepLevel - 1;

                            bool es = true;
                            if (step is BR.Logic.ExecutableStep)
                            {
                                es = (step as BR.Logic.ExecutableStep).EnableStatus;
                            } else
                            {
                                var cm = (step as BR.Logic.CommentStep).Text;
                                writeiline(sv, 0, $"// {cm}");
                            }

                            if (!es)
                            {
                                //writeiline(sv, level, $"Step {step.Title} DISABLED");
                                continue;
                            }
                            string text = null;
                            if (step is BR.Logic.ExecutableStep)
                            {
                                var exstep = step as BR.Logic.ExecutableStep;
                                act = exstep.ActivityType.ToString();
                                writeiline(sv, level, $"{act}");
                                if (exstep.SelectedOptionId != null)
                                {
                                    var pl = exstep.ParametersOptions[exstep.SelectedOptionId.Value];
                                    //writeiline(sv, level, $"Opt: {exstep.SelectedOptionId}");
                                    writeiline(sv, level+1, $"Opt {exstep.SelectedOptionId}: {pl}");
                                }
                                //var pl = exstep.ParametersOptions;
                                //foreach(var p in pl)
                                //{
                                //    writeiline(sv, level+1, $"Opt: {p}");
                                //}
                                stepcounter++;
                            }
                            if (step is BR.Logic.CommentStep)
                            {
                                continue;
                            }

                            int c = step.PropertyValues.Count;
                            //writeiline(sv, level, $"PropCount: {c}");
                            string propstring = "";
                            for (int i = 0; i < c; i++)
                            {
                                string prop = "";
                                try
                                {
                                    if (step.PropertyValues[i].PropertyName == "Parameters")
                                    {
                                        var a = step.PropertyValues[i].Value as ExpressionArguments;
                                        writeiline(sv, level + 1, $"Parameters {a.Count}:");
                                        foreach (var arg in a)
                                        {
                                            string args = $"{arg.ArgumentName} Type: {arg.Type} Value: {arg.Expression}";
                                            prop = args;
                                            writeiline(sv, level + 2, args);
                                        }
                                    }
                                    else
                                    {
                                        //var xp = step.PropertyValues[i].XPathSettings.ToString();
                                        var x2 = step.PropertyValues[i].ToString();
                                        var tp = step.PropertyValues[i].DataType;
                                        var tx = $" : {step.PropertyValues[i].GetType()} / {step.PropertyValues[i].ValueRepresentation}";
                                        propstring = $@"{step.PropertyValues[i].PropertyName}  {x2}";
                                        prop = propstring;
                                        writeiline(sv, level + 1, propstring);
                                    }
                                } catch(Exception ex)
                                {
                                    sv.WriteLine("Exception:" + ex.Message + " at " + act + " prop " + prop);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            sv.WriteLine("Exception:" + ex.Message + " at " + act);
                        }
                        //sv.WriteLine("");
                    }
                    //sv.WriteLine("");
                    //writeiline(sv, 0, $"Script: {s.Name} Parameters {sparams.Count}:");
                    //foreach (var param in sparams)
                    //{
                    //    writeiline(sv, 1, $"{param.Name} Type: {param.Type}  Value: {param.Value}");
                    //}

                }
                catch (Exception ex)
                {
                    sv.WriteLine("Exception:" + ex.Message + "\n" + ex.StackTrace);
                }
                sv.Close();
                ScStrLogic = sv.ToString();
            }
            finally
            {
                sv.Close();
                StepCounter = stepcounter;
            }
        }
    }
}
