﻿using System;
using BR.Core;
using BR.Core.Attributes;

namespace Activities.TL
{
    [ScreenName("PIX Script Walker")]
    [Representation("[ScriptFile] -> Logic File or String")]
    [Path("TL.Utils")]
    [Description("walker converts PIX script to text")]
    [OptionList("To File","To String")]
   
    public class Walker : Activity
    {
        [ScreenName("ScriptFile")]
        [Description("ScriptFile")]
        [IsRequired]
        [IsFilePathChooser]
        [Options(0, 1)]
        public string ScriptFile { get; set; }

        [ScreenName("Logic")]
        [Description("Logic")]
        [IsFilePathChooser]
        [Options(0)]
        public string ScLogic { get; set; }

        [ScreenName("Logic")]
        [Description("Logic")]
        [Options(1)]
        [IsOut]
        public string ScStrLogic { get; set; }

        private void writeiline(System.IO.TextWriter sv, int level, String s)
        {
            sv.WriteLine(s.PadLeft(s.Length + level * 4));
        } 

        public override void Execute(int? optionID)
        {
            var x = System.IO.File.ReadAllText(ScriptFile);
            var t = BR.Logic.Serializer.GetObjectFromXML<BR.Logic.Script>(x);
            var k = t.Logic.Childs.Count.ToString();

            var w = new BR.Logic.ScriptWalker(t);
            var l = w.AllSteps;
            var s = w.Script;
            int level = 0;

            System.IO.TextWriter sv;
            if (optionID == 0)
            {
                sv = System.IO.File.CreateText(ScLogic);
            }
            else
            {
                sv = new System.IO.StringWriter();
            }
            try
            {
                sv.WriteLine(ScriptFile + ":\n");
                var sparams = s.Params;
                try
                {
                    foreach (var step in l)
                    {
                        try
                        {
                            level = step.StepLevel - 1;

                            bool es = true;
                            if (step is BR.Logic.ExecutableStep)
                            {
                                es = (step as BR.Logic.ExecutableStep).EnableStatus;
                            }

                            if (!es)
                            {
                                writeiline(sv, level, $"Step {step.Title} DISABLED");
                                continue;
                            }
                            writeiline(sv, level, step.Title?.ToString());
                            writeiline(sv, level, $"Repreresentation: {step.Representation}");
                            string text = null;
                            if (step is BR.Logic.ExecutableStep)
                            {
                                var exstep = step as BR.Logic.ExecutableStep;
                                text = exstep.Text;
                                    writeiline(sv, level, $"Type: {exstep.ActivityType.ToString()}");
                            }
                            if (step is BR.Logic.CommentStep)
                            {
                                text = (step as BR.Logic.CommentStep).Text;
                            }

                            writeiline(sv, level, "Text: " + text);
                            writeiline(sv, level, "Properties:");
                            int c = step.PropertyValues.Count;
                            string propstring = "";
                            for (int i = 0; i < c; i++)
                            {
                                if (step.PropertyValues[i].PropertyName == "Parameters")
                                {
                                    var a = step.PropertyValues[i].Value as ExpressionArguments;
                                    writeiline(sv, level + 1, $"Parameters {a.Count}:");
                                    foreach (var arg in a)
                                    {
                                        string args = $"{arg.ArgumentName} Type: {arg.Type} Value: {arg.Expression}";
                                        writeiline(sv, level + 2, args);
                                    }
                                }
                                else
                                {
                                    propstring = $@"Name: {step.PropertyValues[i].PropertyName} Value: {step.PropertyValues[i].Value} Expression: {step.PropertyValues[i].Expression}";
                                    writeiline(sv, level + 1, propstring);
                                }
                            }
                        } catch (Exception ex)
                        {
                            sv.WriteLine("Exception:" + ex.Message + "\n" + ex.StackTrace);
                        }
                    }
                    sv.WriteLine("");
                    writeiline(sv, 0, $"Script: {s.Name} Parameters {sparams.Count}:");
                    foreach (var param in sparams)
                    {
                        writeiline(sv, 1, $"{param.Name} Type: {param.Type}  Value: {param.Value}");
                    }
                    
                }
                catch (Exception ex)
                {
                    sv.WriteLine("Exception:" + ex.Message + "\n" + ex.StackTrace);
                }
                sv.Close();
                ScStrLogic = sv.ToString();
            }
            finally
            {
                sv.Close();
            }
        }
    }
}
