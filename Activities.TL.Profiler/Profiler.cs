﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using BR.Core;
using BR.Core.Attributes;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Activities.TL
{
    class StepInfo
    {
        public string name { get; set; }
        public DateTime st_time { get; set; }
        public DateTime en_time { get; set; }
        public double time { get; set; }
        public int diff { get; set; }
        public int count { get; set; }
    }

    class LineReader
    {
        List<string> files;
        int filenum;
        System.IO.StreamReader st;
        public LineReader(List<string> _files)
        {
            files = _files;
            filenum = -1;
            st = null;

        }

        public string readnext()
        {
            if (files.Count < 1) return null;
            if(filenum < 0) filenum = 0;
            ST:
            if(filenum  < files.Count)
            {
                if (st == null)
                {
                    st = new System.IO.StreamReader(files[filenum], Encoding.GetEncoding(1251));
                }
                if (st.Peek() >= 0)
                {
                    return st.ReadLine();
                } else
                {
                    st.Close();
                    st = null;
                    ++filenum;
                    // perfect so;ution )
                    goto ST;
                }
            }
        
            return null;
        }
    }

    // ...........................................................................................................

    [ScreenName("Simple Profiler")]
    [Representation("Simple Profiler from date [StartDate] to date [EndDate] in [Result] DataTable")]
    [Path("TL.Utils")]
    [Description("Simple Profiler")]
    [OptionList("Studio", "Robot", "File")]
    class Profiler : Activity
    {

        [ScreenName("Start Date")]
        [Description("Start Date")]
        public string StartDate { get; set; }

        [ScreenName("End Date")]
        [Description("End Date")]
        public string EndDate { get; set; }

        [ScreenName("Result")]
        [Description("Result")]
        [IsRequired]
        [IsOut]
        public DataTable Result { get; set; }

        [ScreenName("Debug")]
        [Description("Debug")]
        [IsRequired]
        [IsOut]
        public string Debug { get; set; }

        [IsRequired]
        [Options(0)]
        private string LocST { get; set; }

        [IsRequired]
        [Options(1)]
        private string LocRB { get; set; }

        [ScreenName("FileName")]
        [Description("FileName encoded windows-1251")]
        [IsRequired]
        [IsFilePathChooser]
        [Options(2)]
        public string FileName { get; set; }

        private DateTime st;
        private DateTime en;

        private string st_str;
        private string en_str;

        Dictionary<string, StepInfo> timings = new Dictionary<string, StepInfo>();

        // ...................................................
        // regex
        string pat1, pat;
        Regex reg1, reg;


        public override void Execute(int? optionID)
        {
            if(Result == null)
            {
                Result = new DataTable();
            }

            if(StartDate == null || StartDate == "")
            {
                st = DateTime.Now;
            } else
            {
                st = Convert.ToDateTime(StartDate);
            }
            if (EndDate == null || EndDate == "")
            {
                en = DateTime.Now;
            }
            else
            {
                en = Convert.ToDateTime(EndDate);
            }
            st_str = st.ToString("yyyy-MM-dd");
            en_str = en.ToString("yyyy-MM-dd");
            string dd="?";
            string pth = "";
            if(optionID == 0)
            {
                pth = "Studio";
            } else if(optionID == 1)
            {
                pth = "Robot";
            }
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + $@"\PIX\PIX {pth}";
            CreateRegex();

            List<string> dirfiltered;
            if (optionID == 0 || optionID == 1)
            {
                var dir = System.IO.Directory.GetFiles(path, "*.txt");
                dirfiltered = dir.Where(fname =>
                {
                    string fn = System.IO.Path.GetFileNameWithoutExtension(fname);
                    char[] sep = { ' ' };
                    var afn = fn.Split(sep);
                    if (afn.Length < 2) return false;
                    var datestr = afn[1];
                    dd = datestr;
                    var gt = String.Compare(datestr, st_str);
                    var lt = String.Compare(datestr, en_str);
                    if (gt >= 0 && lt <= 0)
                        return true;
                    return false;
                }).ToList();
            } else
            {
                dirfiltered = new List<string>();
                dirfiltered.Add(FileName);
            }

            var Reader = new LineReader(dirfiltered);

            int cnt = 0;
            while(true)
            {
                string str = Reader.readnext();
                if (str == null) break;
                HandleLine(str);
                cnt++;
            }

            Debug = $"st {st_str}\nen {en_str}\ndd {dd}\n{cnt}";

            var NameColumn = new DataColumn("Name");
            NameColumn.DataType = System.Type.GetType("System.String");
            Result.Columns.Add(NameColumn);

            var DiffColumn = new DataColumn("Diff");
            DiffColumn.DataType = System.Type.GetType("System.Int32");
            Result.Columns.Add(DiffColumn);
            
            var TimeColumn = new DataColumn("Time");
            DiffColumn.DataType = System.Type.GetType("System.String");
            Result.Columns.Add(TimeColumn);

            var CountColumn = new DataColumn("Count");
            CountColumn.DataType = System.Type.GetType("System.Int32");
            Result.Columns.Add(CountColumn);

            var IdColumn = new DataColumn("Id");
            IdColumn.DataType = System.Type.GetType("System.String");
            Result.Columns.Add(IdColumn);

            foreach (var kv in timings)
            {
                DataRow r = Result.NewRow();
                r["Id"] = kv.Key;
                r["Name"] = kv.Value.name;
                r["Diff"] = kv.Value.diff;
                r["Time"] = String.Format("{0:0.00}", kv.Value.time);
                r["Count"] = kv.Value.count;
                Result.Rows.Add(r);
            }
        }

        // ..................................................................................

        void CreateRegex()
        {
            pat1 = @"^\d{4}-";
            reg1 = new Regex(pat1, RegexOptions.Compiled);
            // 2020-11-03 06:45:01.8701 Info Start. Executing step: 'Set value', ID: 4506f0bf-caf5-49d8-a325-d5bf4c0c07fc  
            // 2020-11-03 06:45:01.9471 Info End. Executing step: 'Set value', ID: 4506f0bf-caf5-49d8-a325-d5bf4c0c07fc   
            pat = @"^(\d{4}-\d{2}-\d{2})\s(\d{2}:\d{2}:\d{2})\.(\d{4})\sInfo\s(\w+)\.\sExecuting step:\s\'(.+?)\',\sID:\s([\-0-9a-z]+)";
            reg = new Regex(pat, RegexOptions.Compiled);
        }

        // ..................................................................................

        void HandleLine(string str)
        {
            var b = reg1.IsMatch(str);
            if (!b) return;

            var m = reg.Matches(str);
            if (m.Count < 1) return;
            var g = m[0].Groups;
            if (g.Count < 7) return;

            var d = g[1].Value;     // date
            var t = g[2].Value;     // time
            var ms = g[3].Value;    // ms
            var io = g[4].Value;    // Start|End
            var step = g[5].Value;  // Step name
            var id = g[6].Value;    // Id

            DateTime dtms;
            dtms = Convert.ToDateTime(DateTime.ParseExact($"{d} {t}.{ms}", "yyyy-MM-dd HH:mm:ss.ffff", CultureInfo.InvariantCulture));

            if (io == "Start")
            {
                if (timings.ContainsKey(id))
                {
                    timings[id].st_time = dtms;
                    timings[id].count++;
                }
                else
                {
                    timings[id] = new StepInfo();
                    timings[id].name = step;
                    timings[id].st_time = dtms;
                    timings[id].en_time = dtms;
                    timings[id].count = 1;
                    timings[id].diff = 0;
                }
            }

            if (io == "End")
            {
                if (timings.ContainsKey(id))
                {
                    timings[id].en_time = dtms;
                    var ts = timings[id].en_time.Subtract(timings[id].st_time);
                    timings[id].diff += Convert.ToInt32(ts.TotalMilliseconds);
                    timings[id].time = Convert.ToDouble(timings[id].diff) / timings[id].count;
                }
                else
                {
                    timings[id] = new StepInfo();
                    timings[id].name = step;
                    timings[id].st_time = dtms;
                    timings[id].en_time = dtms;
                    timings[id].count = 1;
                    timings[id].diff = 0;
                    timings[id].time = 0.0;
                }
            }
        }
    }
}
