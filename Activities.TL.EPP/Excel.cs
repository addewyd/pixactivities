﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BR.Core;
using BR.Core.Attributes;
using OfficeOpenXml;
using System.IO;
using System.Data;
using System.Drawing;

namespace Activities.TL.EPP
{
    public class CellIndices
    {
        public int firstrow { get; set; }
        public int firstcol { get; set; }
        public int lastrow { get; set; }
        public int lastcol { get; set; }
        public CellIndices()
        {
            firstrow = 1;
            firstcol = 1;
            lastrow = 1;
            lastcol = 1;

        }

        public CellIndices(int a, int b = 1, int c = 1, int d = 1)
        {
            firstrow = a;
            firstcol = b;
            lastrow = c;
            lastcol = d;
        }

        public override string ToString() { 
        
            return $"fr {firstrow} fc {firstcol} lr {lastrow} lc {lastcol}";
        }
    }
    
    [ScreenName("Create new")]
    [Representation("Create new [Handle]")]
    [BR.Core.Attributes.Path("TL.Excel.EPP")]
    [Description("Create new")]

    public class CreateNew : Activity
    {
        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]
        [IsOut]

        public object Handle { get; set; }
        public override void Execute(int? optionID)
        {
            ExcelPackage e = new ExcelPackage();
            Handle = e;
        }
    }

    [ScreenName("Open")]
    [Representation("Open [FileName]")]
    [BR.Core.Attributes.Path("TL.Excel.EPP")]
    [Description("Open")]

    public class Open : Activity
    {
        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]
        [IsOut]

        public object Handle { get; set; }

        [ScreenName("FileName")]
        [Description("FileName")]
        [IsRequired]
        [IsFilePathChooser]
        public string FileName { get; set; }

        public override void Execute(int? optionID)
        {
            FileInfo fi = new FileInfo(FileName);
            ExcelPackage e = new ExcelPackage(fi);
            Handle = e;
        }
    }

    // ..................................................................................

    [ScreenName("Save As")]
    [Representation("Save [Handle] as [FileName]")]
    [BR.Core.Attributes.Path("TL.Excel.EPP")]
    [Description("Save Excel as")]

    public class SaveAs : Activity
    {
        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]

        public object Handle { get; set; }

        [ScreenName("FileName")]
        [Description("FileName")]
        [IsRequired]
        [IsFilePathChooser]
        public string FileName { get; set; }

        public override void Execute(int? optionID)
        {
            ExcelPackage e = Handle as ExcelPackage;
            FileInfo fi = new FileInfo(FileName);
            e.SaveAs(fi);
        }
    }

    [ScreenName("Save")]
    [Representation("Save [Handle]")]
    [BR.Core.Attributes.Path("TL.Excel.EPP")]
    [Description("Save Excel")]

    // ..................................................................................

    public class Save : Activity
    {
        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]

        public object Handle { get; set; }

        [ScreenName("FileName")]
        [Description("FileName")]
        [IsOut]
        public string FileName { get; set; }

        public override void Execute(int? optionID)
        {
            ExcelPackage e = Handle as ExcelPackage;
            FileName = e.File.FullName;
            e.Save();            
        }
    }

    // ..................................................................................

    [ScreenName("Add worksheet")]
    [Representation("Add [WorkSheet] to [Handle]")]
    [BR.Core.Attributes.Path("TL.Excel.EPP")]
    [Description("Add worksheet")]

    public class AddWS : Activity
    {
        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]

        public object Handle { get; set; }

        [ScreenName("WorkSheet")]
        [Description("WorkSheet")]
        [IsRequired]
        public string WorkSheet { get; set; }

        [ScreenName("WS Handle")]
        [Description("WS Handle")]
        [IsOut]

        public object WSHandle { get; set; }

        public override void Execute(int? optionID)
        {
            ExcelPackage e = Handle as ExcelPackage;
            //var wss = e.Workbook.Worksheets;
            //wss.Count;
            var ws = e.Workbook.Worksheets.Add(WorkSheet);
            // last col  = ws.Dimension.End.Column
            // last row = ws.Dimension.End.Row
            WSHandle = ws;
        }
    }

    // ..................................................................................

    [ScreenName("Workbook Info")]
    [Representation("Workbook Info [Handle]")]
    [BR.Core.Attributes.Path("TL.Excel.EPP")]
    [Description("Workbook Info")]

    public class WBInfo : Activity
    {
        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]
        public object Handle { get; set; }

        [ScreenName("Worksheet count")]
        [Description("Worksheet count")]
        [IsOut]
        public int WorksheetCount { get; set; }

        [ScreenName("Worksheet Names")]
        [Description("Worksheet Names")]
        [IsOut]
        public List<string> WorksheetNames { get; set; } = new List<string>();

        public override void Execute(int? optionID)
        {
            ExcelPackage e = Handle as ExcelPackage;
            var wss = e.Workbook.Worksheets;
            WorksheetCount = wss.Count;
            WorksheetNames.Clear();
            for(int i = 1; i <= wss.Count; i ++)
            {
                WorksheetNames.Add(wss[i].Name);
            }
        }
    }

    // ..................................................................................

    [ScreenName("Worksheet Info")]
    [Representation("Worksheet Info [WSIndex] (from 1) of [Handle]")]
    [BR.Core.Attributes.Path("TL.Excel.EPP")]
    [Description("Worksheet Info")]

    public class WSInfo : Activity
    {
        #region Properties
        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]
        public object Handle { get; set; }

        [ScreenName("WSIndex")]
        [Description("WSIndex")]
        [IsRequired]
        public int WSIndex { get; set; }

        [ScreenName("Worksheet Dimension")]
        [Description("Worksheet Dimension")]
        [IsOut]

        public List<int> Dimensions { get; set; } = new List<int>();

        [ScreenName("Worksheet Dimension (as adress)")]
        [Description("Worksheet Dimension (as adress)")]
        [IsOut]
        public ExcelAddressBase DimensionsN { get; set; }

        [ScreenName("Worksheet Dimension (as indices)")]
        [Description("Worksheet Dimension (as indices)")]
        [IsOut]
        public CellIndices DimensionsI { get; set; }
        #endregion
        public override void Execute(int? optionID)
        {
            ExcelPackage e = Handle as ExcelPackage;
            var ws = e.Workbook.Worksheets[WSIndex];
            int firstcol = 0;
            int firstrow = 0;
            int lastcol = 0;
            int lastrow = 0;

            if (ws.Dimension != null)
            {
                firstcol = ws.Dimension.Start.Column;
                firstrow = ws.Dimension.Start.Row;
                lastcol = ws.Dimension.End.Column;
                lastrow = ws.Dimension.End.Row;

            }
            Dimensions.Clear();
            Dimensions.Add(firstrow);
            Dimensions.Add(firstcol);
            Dimensions.Add(lastrow);
            Dimensions.Add(lastcol);

            DimensionsN = new ExcelAddressBase(firstrow, firstcol, lastrow, lastcol);
            DimensionsI = new CellIndices(firstrow, firstcol, lastrow, lastcol);
        }
    }

    // ..................................................................................

    [ScreenName("Set Cell Values")]
    [Representation("Set [CellValue] to Cell (Range) wb [Handle], ws [WSIndex]")]
    [BR.Core.Attributes.Path("TL.Excel.EPP")]
    [Description("Set Cell Value")]
    [OptionList("Names", "Indices")]

    public class SetCellValue : Activity
    {
        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]
        public object Handle { get; set; }

        [ScreenName("WSIndex")]
        [Description("WSIndex")]
        [IsRequired]
        public int WSIndex { get; set; }

        [ScreenName("Cell Name")]
        [Description("Cell Name")]
        [IsRequired]
        [Options(0)]
        public string CellName { get; set; }
        
        [ScreenName("Cell Indices")]
        [Description("Cell Indices")]
        [IsRequired]
        [Options(1)]
        public CellIndices Indices { get; set; }
        
        [ScreenName("Cell Value")]
        [Description("Cell Value")]
        [IsRequired]
        public object CellValue { get; set; }

        public override void Execute(int? optionID)
        {
            ExcelPackage e = Handle as ExcelPackage;
            var ws = e.Workbook.Worksheets[WSIndex];
            if (optionID == 0)
                ws.Cells[CellName].Value = CellValue;
            else if (optionID == 1)
            {
                ws.Cells[Indices.firstrow, Indices.firstcol, Indices.lastrow, Indices.lastcol].Value = CellValue;
            }
            else
            {
                ;
            }
        }
    }

    // ..................................................................................

    [ScreenName("Get Cell Values")]
    [Representation("Get Cell (Range) from wb [Handle], ws [WSIndex]")]
    [BR.Core.Attributes.Path("TL.Excel.EPP")]
    [Description("Get Cell Value")]
    [OptionList("To object (Names)", "To DataTable (Names)", "To object (Indices)", "To DataTable (Indices)")]

    public class GellValue : Activity
    {
#region Proprties
        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]
        public object Handle { get; set; }

        [ScreenName("WSIndex")]
        [Description("WSIndex")]
        [IsRequired]
        public int WSIndex { get; set; }

        [ScreenName("Cell Name")]
        [Description("Cell Name")]
        [IsRequired]
        [Options(0,1)]
        public string CellName { get; set; }

        [ScreenName("Cell Indices")]
        [Description("Cell Indices")]
        [IsRequired]
        [Options(2, 3)]
        public CellIndices Indices { get; set; }

        [ScreenName("Cell Value")]
        [Description("Cell Value")]
        [IsOut]
        [Options(0,2)]
        public object Value { get; set; }

        [ScreenName("Cells Table")]
        [Description("Cells Table")]
        [IsOut]
        [Options(1,3)]
        public DataTable Table { get; set; }
#endregion
        public override void Execute(int? optionID)
        {
            ExcelPackage e = Handle as ExcelPackage;
            var ws = e.Workbook.Worksheets[WSIndex];

            ExcelRange cells = null;
            if (optionID == 0 || optionID == 1)
            {
                cells = ws.Cells[CellName];
            }
            else if (optionID == 2 || optionID == 3)
            {
                cells = ws.Cells[Indices.firstrow, Indices.firstcol, Indices.lastrow, Indices.lastcol];
            }

            if (optionID == 0 || optionID == 2)
            {
                Value = cells.Value;//Text;  
            } else if(optionID == 1 || optionID == 3)
            {
                Table = new DataTable();
                if (ws.Dimension == null)
                {
                    return;
                }

                var at = cells;
                
                for (int k = at.Start.Column; k <= at.End.Column;  k ++)
                {
                    //string columnName = $"COL{k}";
                    var a = at.Offset(0, k-1, 1, 1).Address;
                    a = System.Text.RegularExpressions.Regex.Replace(a, @"\d+$", "");
                    Table.Columns.Add(a);
                }

                for (int i = at.Start.Row; i <= at.End.Row; i++)
                {                    
                    var row = ws.Cells[i, at.Start.Column, i, at.End.Column];
                    DataRow newRow = Table.NewRow();
                    foreach (var cell in row)
                    {
                        newRow[cell.Start.Column - 1] = cell.Text;
                    }

                    Table.Rows.Add(newRow);
                }
            }
        }
    }

    // ..................................................................................

    [ScreenName("Set Cell Style")]
    [Representation("Set style to Cell (Range) wb [Handle], ws [WSIndex]")]
    [BR.Core.Attributes.Path("TL.Excel.EPP")]
    [Description("Set Cell Style")]
    [OptionList("Names", "Indices")]

    public class SetCellStyle : Activity
    {
        #region Properties
        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]
        public object Handle { get; set; }

        [ScreenName("WSIndex")]
        [Description("WSIndex")]
        [IsRequired]
        public int WSIndex { get; set; }

        [ScreenName("Cell Name")]
        [Description("Cell Name")]
        [IsRequired]
        [Options(0)]
        public string CellName { get; set; }

        [ScreenName("Cell Indices")]
        [Description("Cell Indices")]
        [IsRequired]
        [Options(1)]
        public CellIndices Indices { get; set; }

        [ScreenName("Fill Pattern")]
        [Description("Fill Pattern")]
        public OfficeOpenXml.Style.ExcelFillStyle FillStyle { get; set; }

        [ScreenName("Fill Color")]
        [Description("Fill Color")]
        public String FillColor { get; set; } = null; // "#FFFFFF";

        [ScreenName("Font Size")]
        [Description("Font Size")]
        public Int32? FontSize { get; set; } = null;

        [ScreenName("Font Name")]
        [Description("Font Name")]
        public string FontName { get; set; } = null;

        [ScreenName("Font Color")]
        [Description("Font Color")]
        public string FontColor { get; set; } = null;

        [ScreenName("Format")]
        [Description("Format")]
        public string Format { get; set; } = null;

        [ScreenName("Bold")]
        [Description("Bold (true, false or null)")]
        public bool? Bold { get; set; } = null;

        [ScreenName("Italic")]
        [Description("Italic (true, false or null)")]
        public bool? Italic { get; set; } = null;

        [ScreenName("Underline")]
        [Description("Underline (true, false or null)")]
        public bool? Underline { get; set; } = null;
        #endregion
        public override void Execute(int? optionID)
        {
            ExcelPackage e = Handle as ExcelPackage;
            var ws = e.Workbook.Worksheets[WSIndex];

            ExcelRange cells = null;
            if (optionID == 0)
            {
                cells = ws.Cells[CellName];
            } else
            {
                cells = ws.Cells[Indices.firstrow, Indices.firstcol, Indices.lastrow, Indices.lastcol];
            }

            //if(FillStyle != OfficeOpenXml.Style.ExcelFillStyle.None)
            cells.Style.Fill.PatternType = FillStyle;

            if (FillColor != null)
            {
                cells.Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(FillColor));

            }
            if (FontSize != null) {
                cells.Style.Font.Size = FontSize.Value;
            }

            if (FontName != null)
            {
                cells.Style.Font.Name = FontName;
            }

            if (FontColor != null)
            {
                cells.Style.Font.Color.SetColor(ColorTranslator.FromHtml(FontColor));

            }
            if (Format != null)
            {
                cells.Style.Numberformat.Format = Format;
            }
            if (Bold != null)
            {
                cells.Style.Font.Bold = Bold.Value;
            }
            if (Italic != null)
            {
                cells.Style.Font.Italic = Italic.Value;
            }
            if (Underline != null)
            {
                cells.Style.Font.UnderLine = Underline.Value;
            }
        }
    }

    // ..................................................................................

    [ScreenName("Set Cell Border Style")]
    [Representation("Set border style to Cell (Range) wb [Handle], ws [WSIndex]")]
    [BR.Core.Attributes.Path("TL.Excel.EPP")]
    [Description("Set Cell Border Style")]
    [OptionList("Names", "Indices")]

    public class SetCellBorderStyle : Activity
    {
        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]
        public object Handle { get; set; }

        [ScreenName("WSIndex")]
        [Description("WSIndex")]
        [IsRequired]
        public int WSIndex { get; set; }

        [ScreenName("Cell Name")]
        [Description("Cell Name")]
        [IsRequired]
        [Options(0)]
        public string CellName { get; set; }

        [ScreenName("Cell Indices")]
        [Description("Cell Indices")]
        [IsRequired]
        [Options(1)]
        public CellIndices Indices { get; set; }

        [ScreenName("Top Border Style")]
        [Description("Top Border Style")]
        public OfficeOpenXml.Style.ExcelBorderStyle TopBorderStyle { get; set; }

        [ScreenName("Right Border Style")]
        [Description("Rigfht Border Style")]
        public OfficeOpenXml.Style.ExcelBorderStyle RightBorderStyle { get; set; }

        [ScreenName("Bottom Border Style")]
        [Description("Bottom Border Style")]
        public OfficeOpenXml.Style.ExcelBorderStyle BottomBorderStyle { get; set; }

        [ScreenName("Left Border Style")]
        [Description("Left Border Style")]
        public OfficeOpenXml.Style.ExcelBorderStyle LeftBorderStyle { get; set; }

        [ScreenName("Top Border Color")]
        [Description("Top Border Color")]
        public string TopBorderColor { get; set; } = null;

        [ScreenName("Right Border Color")]
        [Description("Rigfht Border Color")]
        public string RightBorderColor { get; set; } = null;

        [ScreenName("Bottom Border Color")]
        [Description("Bottom Border Color")]
        public string BottomBorderColor { get; set; } = null;

        [ScreenName("Left Border Color")]
        [Description("Left Border Color")]
        public string LeftBorderColor { get; set; } = null;


        public override void Execute(int? optionID)
        {
            ExcelPackage e = Handle as ExcelPackage;
            var ws = e.Workbook.Worksheets[WSIndex];
            ExcelRange cells = null;
            if (optionID == 0)
            {
                cells = ws.Cells[CellName];
            }
            else
            {
                cells = ws.Cells[Indices.firstrow, Indices.firstcol, Indices.lastrow, Indices.lastcol];
            }

            cells.Style.Border.Top.Style = TopBorderStyle;
            cells.Style.Border.Right.Style = RightBorderStyle;
            cells.Style.Border.Bottom.Style = BottomBorderStyle;
            cells.Style.Border.Left.Style = LeftBorderStyle;
            if(TopBorderColor != null)
            {
                cells.Style.Border.Top.Color.SetColor(ColorTranslator.FromHtml(TopBorderColor));
            }

            if (RightBorderColor != null)
            {
                cells.Style.Border.Right.Color.SetColor(ColorTranslator.FromHtml(RightBorderColor));
            }
            if (BottomBorderColor != null)
            {
                cells.Style.Border.Bottom.Color.SetColor(ColorTranslator.FromHtml(BottomBorderColor));
            }
            if (LeftBorderColor != null)
            {
                cells.Style.Border.Left.Color.SetColor(ColorTranslator.FromHtml(LeftBorderColor));
            }

        }
    }

    // ..................................................................................

    [ScreenName("Set Column Width")]
    [Representation("Set Column [ColIndex] Width to [Width]  wb [Handle], ws [WSIndex]")]
    [BR.Core.Attributes.Path("TL.Excel.EPP")]
    [Description("Set Column Width")]

    public class SetColWidth : Activity
    {
        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]
        public object Handle { get; set; }

        [ScreenName("WSIndex")]
        [Description("WSIndex")]
        [IsRequired]
        public int WSIndex { get; set; }

        [ScreenName("ColIndex")]
        [Description("ColIndex")]
        [IsRequired]
        public int ColIndex { get; set; }

        [ScreenName("Width")]
        [Description("Width")]
        [IsRequired]
        public Int32  Width { get; set; }

        public override void Execute(int? optionID)
        {
            ExcelPackage e = Handle as ExcelPackage;
            var ws = e.Workbook.Worksheets[WSIndex];
            ws.Column(ColIndex).Width = Width;
        }
    }

    // ..................................................................................

    [ScreenName("Set Cell Alignment")]
    [Representation("Set Cell Alignment wb [Handle], ws [WSIndex]")]
    [BR.Core.Attributes.Path("TL.Excel.EPP")]
    [Description("Set Cell Alignment")]
    [OptionList("Names", "Indices")]

    public class SetAlignment : Activity
    {
        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]
        public object Handle { get; set; }

        [ScreenName("WSIndex")]
        [Description("WSIndex")]
        [IsRequired]
        public int WSIndex { get; set; }

        [ScreenName("Cell Name")]
        [Description("Cell Name")]
        [IsRequired]
        [Options(0)]
        public string CellName { get; set; }

        [ScreenName("Cell Indices")]
        [Description("Cell Indices")]
        [IsRequired]
        [Options(1)]
        public CellIndices Indices { get; set; }

        [ScreenName("Horizontal Alignment")]
        [Description("Horizontal Alignment")]
        public OfficeOpenXml.Style.ExcelHorizontalAlignment HorizontalAlignment { get; set; }

        [ScreenName("Verical Alignment")]
        [Description("Verical Alignment")]
        public OfficeOpenXml.Style.ExcelVerticalAlignment VerticalAlignment { get; set; }

        public override void Execute(int? optionID)
        {
            ExcelPackage e = Handle as ExcelPackage;
            var ws = e.Workbook.Worksheets[WSIndex];
            ExcelRange cells = null;
            if (optionID == 0)
            {
                cells = ws.Cells[CellName];
            }
            else
            {
                cells = ws.Cells[Indices.firstrow, Indices.firstcol, Indices.lastrow, Indices.lastcol];
            }

            cells.Style.HorizontalAlignment = HorizontalAlignment;
            cells.Style.VerticalAlignment = VerticalAlignment;
        }
    }

    // ..................................................................................

    [ScreenName("Formula")]
    [Representation("Formula in [CellName]  wb [Handle], ws [WSIndex]")]
    [BR.Core.Attributes.Path("TL.Excel.EPP")]
    [Description("Formula")]

    public class SetFormula : Activity
    {
        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]
        public object Handle { get; set; }

        [ScreenName("WSIndex")]
        [Description("WSIndex")]
        [IsRequired]
        public int WSIndex { get; set; }

        [ScreenName("Cell Name")]
        [Description("Cell Name")]
        [IsRequired]
        public string CellName { get; set; }

        [ScreenName("Formula")]
        [Description("Formula")]
        public string Formula { get; set; } = null;

        public override void Execute(int? optionID)
        {
            ExcelPackage e = Handle as ExcelPackage;
            var ws = e.Workbook.Worksheets[WSIndex];

            if (Formula != null)
                ws.Cells[CellName].Formula = Formula;

        }
    }

    // ..................................................................................

    [ScreenName("Add Image")]
    [Representation("Add Image [ImagePath] wb [Handle], ws [WSIndex]")]
    [BR.Core.Attributes.Path("TL.Excel.EPP")]
    [Description("Add Image")]

    public class AddImage : Activity
    {
        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]
        public object Handle { get; set; }

        [ScreenName("WSIndex")]
        [Description("WSIndex")]
        [IsRequired]
        public int WSIndex { get; set; }

        [ScreenName("ImagePath")]
        [Description("ImagePath")]
        [IsRequired]
        [IsFilePathChooser]
        public string ImagePath { get; set; }

        [ScreenName("Cell Indices")]
        [Description("Cell Indices (row, col, offset, offset)")]
        [IsRequired]
        public CellIndices Indices { get; set; }

        public override void Execute(int? optionID)
        {
            ExcelPackage e = Handle as ExcelPackage;
            var ws = e.Workbook.Worksheets[WSIndex];
            using (System.Drawing.Image image = System.Drawing.Image.FromFile(ImagePath))
            {
                var excelImage = ws.Drawings.AddPicture(System.IO.Path.GetFileName(ImagePath), image);

                excelImage.SetPosition(Indices.firstrow - 1, Indices.lastrow, Indices.firstcol, Indices.lastcol);
            }
        }
    }

    // ..................................................................................

    [ScreenName("Merge Cells")]
    [Representation("Merge Cells wb [Handle], ws [WSIndex]")]
    [BR.Core.Attributes.Path("TL.Excel.EPP")]
    [Description("Merge Cells")]
    [OptionList("Names", "Indices")]

    public class MergeCells : Activity
    {
        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]
        public object Handle { get; set; }

        [ScreenName("WSIndex")]
        [Description("WSIndex")]
        [IsRequired]
        public int WSIndex { get; set; }

        [ScreenName("Cell Name")]
        [Description("Cell Name")]
        [IsRequired]
        [Options(0)]
        public string CellName { get; set; }

        [ScreenName("Cell Indices")]
        [Description("Cell Indices")]
        [IsRequired]
        [Options(1)]
        public CellIndices Indices { get; set; }

        public override void Execute(int? optionID)
        {
            ExcelPackage e = Handle as ExcelPackage;
            var ws = e.Workbook.Worksheets[WSIndex];

            ExcelRange cells = null;
            if (optionID == 0)
            {
                ws.Cells[CellName].Merge = true;
            }
            else if (optionID == 1)
            {
                ws.Cells[Indices.firstrow, Indices.firstcol, Indices.lastrow, Indices.lastcol].Merge = true;
            }
            
        }
    }

    // todo
    // ws index by name
    // ws name by index
    // range indices by range name
    // vice versa

}
