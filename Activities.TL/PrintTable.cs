﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BR.Core;
using BR.Core.Attributes;
using System.Data;


namespace Activities.TL
{
    [ScreenName("Print Table")]
    [Representation("Print Table [Table] to [OutputString]")]
    [Path("TL.Utils")]
    [Description("Print Table")]

    class PrintTable :Activity
    {
        [ScreenName("Table")]
        [Description("Table")]
        [IsRequired]
        public DataTable Table { get; set; }
        [ScreenName("OutputString")]
        [Description("OutputString")]
        [IsRequired]
        [IsOut]
        public string OutputString { get; set; }

        public override void Execute(int? opt)
        {
            List<int> MaxLens = new List<int>();
            List<string> Types = new List<string>();

            foreach (DataColumn c in Table.Columns)
            {
                Types.Add(c.DataType.Name);
            }
            for (int ci = 0; ci < Table.Columns.Count; ci++)
            {

                var abc = Table.AsEnumerable().Max<DataRow, int>(x => {
                    return x[Table.Columns[ci].ColumnName].ToString().Length;
                });
                MaxLens.Add(abc);
            }

            StringBuilder sb = new StringBuilder("");
            int k = 0;
            foreach (DataColumn c in Table.Columns)
            {
                int pad = MaxLens[k];
                string d = c.ColumnName.PadRight(c.ColumnName.Length + pad + 2);
                sb.Append(d);
                k++;
            }
            sb.AppendLine("");

            foreach (var row in Table.AsEnumerable())
            {
                int kk = 0;
                foreach (DataColumn c in Table.Columns)
                {
                    int pad = MaxLens[kk];
                    string d;
                    //if (Types[kk] == "Int32" || Types[kk] == "Int64")
                    //{
                    //    d = row[c].ToString().PadLeft(pad + 4);
                    //}
                    //else
                    //{
                        d = row[c].ToString().PadRight(pad + 4);
                    //}
                    sb.Append(d);
                    kk++;
                }
                sb.AppendLine("");
            }
            OutputString = sb.ToString();
        }
    }
}
