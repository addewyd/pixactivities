﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BR.Core;
using BR.Core.Attributes;

namespace Activities.TL
{
    [ScreenName("Logger")]
    [Description("Very primitive logger")]
    [Representation("write [Message] to [LogFile]")]
    [Path("TL.Utils")]
    public class Logger:Activity
    {
        [ScreenName("Logfile")]
        [Description("Logfile")]
        [IsRequired]
        [IsFilePathChooser]
        public string LogFile{ get; set; }

        [ScreenName("Message")]
        [Description("Message")]
        [IsRequired]
        public string Message { get; set; }

        public override void Execute(int? optionID)
        {
           
            var sv = System.IO.File.AppendText(LogFile);
            sv.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + Message);
            sv.Close();
        }
    }
}
