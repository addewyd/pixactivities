﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BR.Core;
using BR.Core.Attributes;
using System.Text.Json;


namespace Activities.TL
{
    [ScreenName("Get property from json object")]
    [Description("Get property from json object with def value")]
    [Representation("[JsonEL], [PropName], [Default] -> [prop]")]
    [Path("TL.Utils")]
    public class GetJsonWE : Activity
    {

        [ScreenName("Json")]
        [Description("Json")]
        [IsRequired]

        public JsonElement JsonEL {get; set; }

        [ScreenName("Property Name")]
        [Description("Property Name")]
        [IsRequired]

        public string PropName { get; set; }

        [ScreenName("Default")]
        [Description("Default")]
        [IsRequired]

        public string Default { get; set; }


        [ScreenName("Property")]
        [Description("Property")]
        [IsRequired]
        [IsOut]

        public string prop { get; set; }

        public override void Execute(int? optionID)
        {
            try
            {
                prop = JsonEL.GetProperty(PropName).ToString();
            } catch(Exception ex)
            {
                prop = Default;
            }
        }
    }
}
