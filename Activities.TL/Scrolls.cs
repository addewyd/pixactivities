﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using BR.Core;
using BR.Core.Attributes;
using System.Drawing;
using Images.Core;
using System.Windows;

namespace Activities.TL
{

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public class _ScrollInfo
    {
        public uint cbSize = 0;
        public uint fMask = 1 | 2 | 4 | 8 | 16;
        public int nMin = 0;
        public int nMax = 0;
        public uint nPage = 0;
        public int nPos = 0;
        public int nTrackPos = 0;
    }

    public class ScrollInfo
    {
        public int Min { get; set; }
        public int Max { get; set; }
        public uint Page { get; set; }
        public int Pos { get; set; }
        public int TrackPos { get; set; }
        public override string ToString()
        {
            return $"Min={Min}; Max={Max}; Page={Page}; Pos={Pos}; Trackpos={TrackPos}";
        }
    }

    [ScreenName("GetScrollPos")]
    [Representation("GetScrollPos of [Element] to [ScrollPos] and [SInfo]")]
    [BR.Core.Attributes.Path("TL.Experimental")]
    [Description("GetScrollPos & Scrollinfo")]
    public class GetScrollPosition : Activity
    {
        const int SIF_RANGE = 0x0001;
        const int SIF_PAGE = 0x0002;
        const int SIF_POS = 0x0004;
        const int SIF_DISABLENOSCROLL = 0x0008;
        const int SIF_TRACKPOS = 0x0010;
        const int SIF_ALL = (SIF_RANGE | SIF_PAGE | SIF_POS | SIF_TRACKPOS);

        const int SB_HORZ = 0;
        const int SB_VERT = 1;
        //#define SB_CTL              2
        //#define SB_BOTH             3

        [DllImport("user32.dll", SetLastError = true)]
        static extern int GetScrollPos(IntPtr hWnd, int sb);

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool GetScrollInfo(IntPtr hWnd, int nbar, [In, Out] _ScrollInfo scrollInfo);

        [ScreenName("UI Element")]
        [Description("UI Element")]
        [IsRequired]
        public Desktop.Core.UIElement Element { get; set; }

        [ScreenName("Scrollbar (0 or 1)")]
        [Description("Scrollbar (0 - Hor, 1 - Ver")]
        [IsRequired]
        public int NSB { get; set; } = SB_VERT;

        [ScreenName("ScrollPos")]
        [Description("ScrollPos")]
        [IsRequired]
        [IsOut]
        public int ScrollPos { get; set; }

        [ScreenName("Handle")]
        [Description("Handle")]
        [IsRequired]
        [IsOut]
        public int Handle { get; set; }

        [ScreenName("Rectangle")]
        [Description("Rectangle")]
        [IsRequired]
        [IsOut]

        public Rectangle Rect { get; set; }

        [ScreenName("Scrollpos Error code")]
        [Description("Scrollpos Error code")]
        [IsRequired]
        [IsOut]
        public int Error { get; set; } = 0;

        [ScreenName("Scroll Info")]
        [Description("Scroll Info")]
        //[IsRequired]
        [IsOut]
        public ScrollInfo SInfo { get; set; } // = new ScrollInfo();

        [ScreenName("Scrollinfo Error code")]
        [Description("Scrollinfo Error code")]
        [IsRequired]
        [IsOut]
        public int ErrorSI { get; set; } = 0;

        // ..............................................................................

        public override void Execute(int? optionID)
        {
            Rect = Element.GetRectangle();
            var hwnd = Desktop.Core.Win32.WindowFromPoint(Rect.Location);
            ScrollPos = GetScrollPos(hwnd, NSB);
            if (ScrollPos == 0)
            {
                Error = Marshal.GetLastWin32Error();
            }
            Handle = (int)hwnd;

            _ScrollInfo si = new _ScrollInfo();
            si.cbSize = (uint)Marshal.SizeOf(si);
            var b = GetScrollInfo(hwnd, NSB, si);

            SInfo = new ScrollInfo()
            {
                Max = si.nMax,
                Min = si.nMin,
                Page = si.nPage,
                Pos = si.nPos,
                TrackPos = si.nTrackPos
            };
            if (!b)
            {
                ErrorSI = Marshal.GetLastWin32Error();
            }
        }
    }
}
