﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using BR.Core;
using BR.Core.Attributes;
//using System.IO;

namespace Activities.TL
{

    [ScreenName("MD5")]
    [Representation("[Hash] = MD5([Input] | [InputBytes])  ")]
    [Path("TL.Cryptography")]
    [Description("MD5")]
    [OptionList("From string", "From byte[]")]
    class MD5 : Activity
    {
        System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
#region Properties
        [ScreenName("Input")]
        [Description("Input")]
        [Options(0)]
        public string Input { get; set; }

        [ScreenName("InputBytes")]
        [Description("InputBytes")]
        [Options(1)]
        public byte[] InputBytes { get; set; }

        [ScreenName("Hash")]
        [Description("Hash")]
        [IsRequired]
        [IsOut]
        public string Hash { get; set; }
#endregion
        public override void Execute(int? opt)
        {
            byte[] bytes;
            if (opt == 0)
            {
                bytes = Encoding.UTF8.GetBytes(Input);
            }
            else
            {
                bytes = InputBytes;
            }
            var hb = md5.ComputeHash(bytes);
            var sb = new StringBuilder();
            foreach (var b in hb)
            {
                sb.Append(b.ToString("X2"));
            }
            Hash = sb.ToString();
        }
    }

    [ScreenName("SHA256")]
    [Representation("[Hash] = SHA256([Input] | [InputBytes])")]
    [Path("TL.Cryptography")]
    [Description("SHA256")]
    [OptionList("From string", "From byte[]")]

    class Sha256 : Activity
    {
        SHA256 sha256 = SHA256.Create();
#region Properties

        [ScreenName("Input")]
        [Description("Input")]
        [IsRequired]
        [Options(0)]
        public string Input { get; set; }

        [ScreenName("InputBytes")]
        [Description("InputBytes")]
        [IsRequired]
        [Options(1)]
        public byte[] InputBytes { get; set; }

        [ScreenName("Hash")]
        [Description("Hash")]
        [IsRequired]
        [IsOut]
        public string Hash { get; set; }
#endregion
        public override void Execute(int? opt)
        {
            byte[] bytes;
            if (opt == 0)
            {
                bytes = Encoding.UTF8.GetBytes(Input);
            }
            else
            {
                bytes = InputBytes;
            }
            var hb = sha256.ComputeHash(bytes);
            var sb = new StringBuilder();
            foreach (var b in hb)
            {
                sb.Append(b.ToString("X2"));
            }
            Hash = sb.ToString();
        }
    }

    [ScreenName("Read Binary File")]
    [Representation("Read [FileName] to [OutputBytes]")]
    [Path("TL.Cryptography")]
    [Description("Read binary file")]
    class ReadBytes : Activity
    {
#region Properties
        [ScreenName("FileName")]
        [Description("FileName")]
        [IsFilePathChooser]
        [IsRequired]
        public string FileName { get; set; }

        [ScreenName("InputBytes")]
        [Description("InputBytes")]
        [IsRequired]
        [IsOut]
        public byte[] OutputBytes { get; set; }
#endregion
        public override void Execute(int? opt)
        {
            OutputBytes = System.IO.File.ReadAllBytes(FileName);
        }
    }
}
