﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BR.Core;
using BR.Core.Attributes;
using System.Text.Json;
using System.Data;


namespace Activities.TL
{
    [ScreenName("JSON to DataTable")]
    [Description("JSON to DataTable")]
    [Representation("[Json] to [Table]")]
    [Path("TL.Utils")]

    class JsonToTable :Activity
    {
        [ScreenName("Json")]
        [Description("Json")]
        [IsRequired]
        public JsonElement Json { get; set; }

        [ScreenName("Table")]
        [Description("Table")]
        [IsRequired]
        [IsOut]

        public DataTable Table { get; set; }
        public override void Execute(int? optionID)
        {
            Table = new DataTable();
            int i = 0;
            foreach(var j in Json.EnumerateArray()) {
                if(i == 0)
                {
                    // TODO: column types
                    foreach(var o in j.EnumerateObject())
                    {
                        var name = o.Name;
                        Table.Columns.Add(name);
                    }
                }
                DataRow r = Table.NewRow();
                foreach(DataColumn c in Table.Columns)
                {
                    string n = c.ColumnName;
                    try
                    {
                        r[n] = j.GetProperty(n).ToString();
                    } catch(Exception e)
                    {
                        r[n] = null;
                    }
                }
                Table.Rows.Add(r);
                i++;
            }
        }
    }
}
