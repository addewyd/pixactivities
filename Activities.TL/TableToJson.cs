﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BR.Core;
using BR.Core.Attributes;
using System.Text.Json;
using System.Data;


namespace Activities.TL
{
    [ScreenName("DataTable to JSON")]
    [Description("DataTable to JSON")]
    [Representation("[Table] to Json")]
    [Path("TL.Utils")]
    [OptionList("To String", "To JsonElement")]

    class TableToJson : Activity
    {
        [ScreenName("Table")]
        [Description("Table")]
        [IsRequired]
        [Options(0,1)]
        public DataTable Table { get; set; }

        [ScreenName("JsonString")]
        [Description("JsonString")]
        [IsRequired]
        [IsOut]
        [Options(0)]
        public string JsonString { get; set; }

        [ScreenName("JsonObject")]
        [Description("JsonObject")]
        [IsRequired]
        [IsOut]
        [Options(1)]
        public JsonElement JsonObject { get; set; }

        public override void Execute(int? optionID)
        {
            var a = new List<Dictionary<string, object>>();
            foreach(var r in Table.AsEnumerable())
            {
                var d = new Dictionary<string, object>();
                foreach(DataColumn c in Table.Columns)
                {
                    var name = c.ColumnName;
                    var type = c.DataType.Name;
                    d.Add(name, r[name]);
                }
                a.Add(d);
            }
            var options = new JsonSerializerOptions
            {
                WriteIndented = true
            };
            // option 0
            JsonString = JsonSerializer.Serialize<List<Dictionary<string, object>>>(a, options);
            // option 1
            JsonObject = JsonSerializer.Deserialize<JsonElement>(JsonString);
        }
    }
}
