﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BR.Core;
using BR.Core.Attributes;
using System.Net.Http;

namespace Activities.TL
{
    public static class AsyncHelper
    {
        private static readonly TaskFactory _taskFactory = new
            TaskFactory(CancellationToken.None,
                        TaskCreationOptions.None,
                        TaskContinuationOptions.None,
                        TaskScheduler.Default);

        public static TResult RunSync<TResult>(Func<Task<TResult>> func)
            => _taskFactory
                .StartNew(func)
                .Unwrap()
                .GetAwaiter()
                .GetResult();

        public static void RunSync(Func<Task> func)
            => _taskFactory
                .StartNew(func)
                .Unwrap()
                .GetAwaiter()
                .GetResult();
    }

    // .............................................................................................................

    [ScreenName("Reporter")]
    [Description("Reporter")]
    [Path("TL.HTTP")]
    [Representation("Reporter")]
    [OptionList("Start", "Report", "Stop", "Clear", "GetResponse")]
    [IsManager]
    class Reporter : Activity
    {

        static Thread t = null;
        static bool run = false;
        static System.IO.TextWriter sv;
        static int counter = 0;
        static int wait = 0;
        static int port;
        static string host = "";
        static string resp = "";
        static string postDataPath = "x";
        static Dictionary<string, object> storage = new Dictionary<string, object>();
        static List<string> aux = new List<string>();

        static private Queue<string> RespQueue = new Queue<string>(32);

        static readonly HttpClient client = new HttpClient();
        // static HttpResponseMessage response = null;
        static private readonly object _Lock = new object();
        static private readonly object _LockQ = new object();
        static string guid = null;

        static bool debug = false;
        bool paused = false;
        const int MaxRQCount = 80;

        System.Threading.CancellationToken ct;
        BR.Engine.Helper.PauseToken pt;

        // ....................................................................................................................

        public static void log(string msg)
        {
            if (!debug) return;
            try
            {
                var w = System.IO.File.AppendText("_log");
                try
                {
                    w.WriteLine($"{msg}");
                }
                finally
                {
                    w.Close();
                }
            }
            catch (Exception ex) { }
        }

        // .............................................................................................................

        public static void SendFile()
        {
            if (!debug) return;
            try
            {
                sv = System.IO.File.AppendText("_report");
                try
                {
                    sv.WriteLine($"{counter}");
                    lock (_Lock)
                    {
                        foreach (var k in storage.Keys)
                        {
                            sv.Write($"Sent {k}: {storage[k].ToString()}\t");
                        }
                    }
                    sv.WriteLine("");
                }
                finally
                {
                    sv.Close();
                }
            }
            catch (Exception x) { }
        }

        // .............................................................................................................

        static void Send()
        {
            aux.Clear();
            string data = "";
            try
            {
                lock (_Lock)
                {
                    foreach (var k in storage.Keys)
                    {
                        aux.Add($@"""{k}"": ""{storage[k]}""");
                    }
                    data += String.Join(", ", aux);
                }
            }
            catch (Exception ex)
            {
                log(ex.Message);
            }
            //string url = $"http://localhost:{port}/x/{{{data}}}";
            string url = $"http://{host}:{port}/{postDataPath}";
            HttpResponseMessage response = null;
            try
            {
                var hc = new StringContent(
                    $"{{{data}}}", Encoding.UTF8, "application/json");
                response = AsyncHelper.RunSync(() =>
                {
                    return client.PostAsync(url, hc);
                });
                response.EnsureSuccessStatusCode();
                //log($"Posted: {{{data}}}");
                resp = AsyncHelper.RunSync(() => { return response.Content.ReadAsStringAsync(); });

                lock (_LockQ)
                {
                    if (!RespQueue.Contains(resp))
                    {
                        if(RespQueue.Count > MaxRQCount) 
                        {
                            RespQueue.Dequeue();
                        }
                        RespQueue.Enqueue(resp);
                    }
                }
                //log($"Response: {resp}");
            }
            catch (Exception ex)
            {
                log(ex.Message + " " + url + " " + response?.Content.Headers.ToString() + " " + response?.StatusCode);
            }
        }

        // ......................................................................................................................

        static string RegisterThread()
        {
            var rf = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + $@"\PIX\{guid}";
            try
            {
                if (System.IO.File.Exists(rf))
                {
                    throw new Exception("Thread already running");
                }
                var st = System.IO.File.CreateText(rf);
                var pid = System.Diagnostics.Process.GetCurrentProcess().Id;
                st.WriteLine($"{pid}");
                st.Close();
            } catch(Exception ex)
            {
                log(ex.Message);
            }
            return rf;
        }

        // ......................................................................................................................

        static void UnRegisterThread(string rf)
        {
            // try
            System.IO.File.Delete(rf);
        }

        // ......................................................................................................................

        public /*static*/ void Proc()
        {
            string fn = RegisterThread();
            while (run)
            {
                if(ct.IsCancellationRequested)
                {
                    run = false;
                    log("Cancel request");
                    break;
                }
                if(! paused && pt.IsPausedRequested)
                {
                    paused = true;
                    log("Pause request");                    
                    Thread.Sleep(1000);
                    continue;
                }
                if(paused && !pt.IsPausedRequested)
                {
                    paused = false;
                    log("Resume");
                }

                SendFile();
                Send();
                counter++;
                Thread.Sleep(wait);
            }
            UnRegisterThread(fn);
        }
        #region Properties
        [ScreenName("Host")]
        [Description("HTTP Host")]
        [IsRequired]
        [Options(0)]
        public string Host { get; set; } = "localhost";

        [ScreenName("Port")]
        [Description("HTTP Port")]
        [IsRequired]
        [Options(0)]
        public int Port { get; set; }

        [ScreenName("PostDataPath")]
        [Description("PostDataPath")]
        [IsRequired]
        [Options(0)]
        public string PostDataPath { get; set; } = "x";

        [ScreenName("Wait")]
        [Description("Wait (ms)")]
        [IsRequired]
        [Options(0)]
        public int Wait { get; set; } = 0;

        [ScreenName("Name")]
        [Description("Name")]
        [IsRequired]
        [Options(1)]
        public string RName { get; set; }

        [ScreenName("Value")]
        [Description("Value")]
        [IsRequired]
        [Options(1)]
        public object Value { get; set; }

        [ScreenName("Response")]
        [Description("Response")]
        [IsOut]
        [Options(4)]
        // Options(1,4) //send & getresponse
        public string Response { get; set; }

        [ScreenName("Debug")]
        [Description("Debug")]
        [IsCheckBox]
        [Options(0)]
        public bool Debug { get; set; } = true;
        #endregion
        protected static void KHandler(object sender, ConsoleCancelEventArgs args)
        {
            log("Cancel");
            run = false;
            //Console.CancelKeyPress -= KHandler;
        }

        // ...........................................................................................................

        public override void Execute(int? optionID)
        {
            if (guid == null)
            {
                guid = Guid.NewGuid().ToString();
            }
            try
            {
                switch (optionID)
                {
                    case 0:
                        wait = Wait;
                        port = Port;
                        host = Host;
                        debug = Debug;

                        ct = CancellationToken;
                        pt = PauseToken;

                        lock (_LockQ)
                        {
                            RespQueue.Clear();
                        }

                        Console.CancelKeyPress += new ConsoleCancelEventHandler(KHandler);
                        postDataPath = PostDataPath;
                        try
                        {
                            if (t != null)
                            {
                                log("Aborting...");
                                t.Abort();
                            }
                        }
                        catch (Exception ex)
                        {
                            log(ex.Message);
                        }
                        t = new Thread(new ThreadStart(Proc));
                        run = true;
                        counter = 0;
                        t.Start();
                        break;

                    // Report
                    case 1:
                        lock (_Lock)
                        {
                            storage[RName] = Value;
                            //Response = resp;
                        }
                        break;

                    case 2:
                        run = false;
                        t.Join();
                        break;

                    case 3:
                        lock (_Lock)
                        {
                            storage.Clear();
                        }
                        break;

                    case 4:
                        Response = resp;//;  AsyncHelper.RunSync(() => { return response.Content.ReadAsStringAsync(); });

                        lock(_LockQ)
                        {
                            if(RespQueue.Count > 0)
                            {
                                string r = RespQueue.Dequeue();
                                Response = r;
                            }
                        }

                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                log("main " + ex.Message);
            }
        }
    }
}
