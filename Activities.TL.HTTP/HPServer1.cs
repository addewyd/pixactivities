﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using BR.Core;
using BR.Core.Attributes;

namespace Activities.TL
{
    [ScreenName("HTTP listener")]
    [Representation("HTTP listener at [Port] returns [Response] to client")]
    [Path("TL.HTTP")]
    [Description("HTTP listener")]

    class HPServer : Activity
    {
        TcpListener server = null;

        [ScreenName("Port")]
        [Description("Port")]
        [IsRequired]
        public int Port { get; set; }

        [ScreenName("Request")]
        [Description("Request")]
        [IsRequired]
        [IsOut]
        public string Request { get; set; }

        [ScreenName("Response")]
        [Description("Response")]
        [IsRequired]
        public string Response { get; set; }

        public override void Execute(int? optionID)
        {
            try
            {
                Int32 port = Port;

                var ip = IPAddress.Any;
                server = new TcpListener(ip, port);

                server.Start();

                Byte[] bytes = new Byte[256];

                System.Net.ServicePointManager.Expect100Continue = false;

                TcpClient client = server.AcceptTcpClient();

                var stream = client.GetStream();
                var writer = new System.IO.StreamWriter(client.GetStream());
                int i;
                string r = null;
                string response = Response;
                writer.Write("HTTP/1.0 200 OK");
                writer.Write("\r\n");
                writer.Write("Content-Type: text/plain; charset=UTF-8");
                writer.Write("\r\n");
                writer.Write("Content-Length: " + response.Length);
                writer.Write("\r\n");
                writer.Write("\r\n");
                writer.Write(response);
                writer.Flush();
                //Request = r;
                r = "";
                while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    r += System.Text.Encoding.UTF8.GetString(bytes, 0, i);
                }
                Request = r;
            }
            catch (SocketException e)
            {
                //Console.WriteLine("SocketException: {0}", e);
                throw;
            }
            finally
            {
                server.Stop();
            }
        }
    }
}